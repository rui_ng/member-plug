<?php

namespace sffi\service;

/**
 * Class YiSuGuanJia
 * @method array uploadOrder($house_code, $order_code, $order_time, $plan_checkin_date, $plan_leave_date, $rp_nickname) 更新网约房订单信息
 * @method array uploadPlanCheckIn($house_code, $id, $order_code, $pcname) 网约房拟入住人员信息
 * @method array uploadCheckIn($house_code, $id, $order_code, $checkin_date, $leave_date, $name, $paper_code, $folk, $head_image, $is_pass, $mode, $reg_time, $similarity, $snapshot) 网约房实际入住人员信息
 * @package sffi\service
 */
class YiSuGuanJia extends Base
{
    protected $argUploadOrder = [
        'house_code', // 平台服务商的网约房编码
        'order_code', // 平台服务商的订单编码
        'order_time', // 下单时间 eg:20220805160000
        'plan_checkin_date', // 预订入住日期 eg:20220806
        'plan_leave_date', // 拟离开日期 eg:20220806
        'rp_nickname' // 预订人昵称
    ];

    protected $argUploadPlanCheckIn = [
        'house_code', // 平台服务商的网约房编码
        'id', // 平台服务商拟入住员编码(关健字)
        'order_code', // 平台服务商的订单编码
        'pcname'  // 姓名
    ];

    protected $argUploadCheckIn = [
        'house_code', // 平台服务商的网约房编码
        'id', // 平台服务商拟入住员编码(关健字)
        'order_code', // 平台服务商的订单编码(关健字)
        'checkin_date', // 入住日期 eg:20220806
        'leave_date', // 离开日期 eg:20220806
        'name', // 姓名
        'paper_code', // 身份证号码
        'folk', // 预订人民族
        'head_image', // 身份证件的头像照片 base64 字符串，头像照片文件需小于 2MB
        'is_pass', // 人证核验结果是否通过 true false
        'mode', // 证件基本信息采集模式 0 证件读卡 1 OCR识别 2 手动录
        'reg_time', // 登记时间 eg:20220805160000
        'similarity', // 人像比对相似度（0~1），大于 0 小于
        'snapshot', // 现场抓拍的人像图片，base64 字符串
    ];
}
